/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author admin
 */
public class ChatMessage implements Serializable {
    
    private Long id;
    
    private ChatUser user;
    
    private String message;
    
    public ChatMessage(ChatUser chU) {
        this.user = chU;
    }
    
    public ChatMessage() {
        
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public ChatUser getUser() {
        return user;
    }

    public void setUser(ChatUser user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Id:"+ this.id+ "\nPseudo:" + this.user.getPseudo() + "\nMessage:"+this.message;
    }
    
}
