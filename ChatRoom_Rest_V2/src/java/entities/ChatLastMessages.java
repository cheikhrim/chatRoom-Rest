/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class ChatLastMessages implements Serializable {
    
    private ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();
    
    /*
    @Override
    public String toString() {
        return "Message[ user=" + this.user + ", message=\""+this.message+"\" ]";
    }
    */

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }
    
    
}
