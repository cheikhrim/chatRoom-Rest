/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

import entities.ChatLastMessages;
import entities.ChatMessage;
import entities.ChatUser;
import entities.Message;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author admin
 */
@Path("chatroom")
public class ChatroomResource {

    private ArrayList<ChatUser> usersList = new ArrayList<ChatUser>();
    private ArrayList<ChatMessage> messagesList = new ArrayList<ChatMessage>();
    
    @Context
    private UriInfo context;
    
    @Inject
    private Executor executor;
    
    @GET
    @Path("/async")
    public void asyncGet(@Suspended final AsyncResponse asyncResponse) {
        executor.execute(() -> {
             //String result = usersList;
             asyncResponse.resume(usersList);
        });
    }

    /**
     * Creates a new instance of ChatroomResource
     */
    public ChatroomResource() {
    }

    /**
     * Retrieves representation of an instance of resource.ChatroomResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of ChatroomResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @POST
    @Path("/subscribe")
    @Consumes("application/json")
    public Response subscribe(ChatUser user) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //this.usersList.add(user);
        System.out.println("Bienvenue "+ user.getPseudo());
        //ChatUser chUser = new ChatUser();
        //chUser.setPseudo(pseudo);
        addUser(user, "Users.txt");
        return Response
                    .status(201)
                    .entity(user.toString())
                    .build();
        /*return Response
          .status(201)
          .entity(user.toString())
          .build();
        */
    }

    @POST
    @Path("/unsubscribe")
    @Consumes("application/json")
    public Response unsubscribe(String pseudo) {
        ChatUser chUser = new ChatUser();
        chUser.setPseudo(pseudo);
        addUser(new ChatUser(pseudo), "Users.txt");
        return Response
                    .status(201)
                    .entity(chUser.toString())
                    .build();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        /*
        for (int i =0; i<this.usersList.size(); i++){
           if (this.usersList.get(i).getPseudo().equals(pseudo)) {
               this.usersList.remove(i);
               return Response
                    .status(201)
                    .entity(this.usersList.get(i).getId())
                    .build();
           }
        }*/
        /*
        return Response
                .status(500)
                .build();
        */
    }

    @POST
    @Path("/postMessage")
    @Consumes("application/json")
    public Response postMessage(Message m) {
        
        ChatUser user = new ChatUser();
        /*
        }*/
        
        this.usersList = new ArrayList<ChatUser>();
        ArrayList<ChatUser> chU = new ArrayList<ChatUser>();
        this.usersList = getUsers("Users.txt");
        for (int i =0; i<this.usersList.size(); i++){
            if (this.usersList.get(i).getPseudo().equals(m.getPseudo())) {
               user = this.usersList.get(i);
               break;
            }
        }
        
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessage(m.getMessage());
        chatMessage.setUser(user);
        
        //this.messagesList.add(chatMessage);
        if (addMessage(chatMessage, "TestPseudo.txt", "TestMessage.txt"))
            System.out.print("true");
        else
            System.out.print("false");
        //System.out.println(m.getMessage());
        /*for (int j=0; j<messagesList.size(); j++) {
            System.out.println(messagesList.get(j).getMessage());
        }*/
        System.out.println(m.getMessage());
        
        // Try to see whats inside our file
        //ChatLastMessages chLM = getLMessagesV2("Test.txt");
        //System.out.println("Size after writing : "+chLM.getMessages().size());
        return Response
                .status(201)
                .build();
    }
 
    @GET
    @Path("/lastMessages/{from}")
    public ChatLastMessages getLastMessages(@PathParam("from") String from) {
        //System.out.println(0);
        /*if (Integer.parseInt(from)>=messagesList.size())
            return new ChatLastMessages();*/
        
        /*System.out.println(messagesList.size());
        ArrayList<ChatMessage> lastMessages = new ArrayList<ChatMessage>();
        for (int i=Integer.parseInt(from); i<messagesList.size();i++) {
            lastMessages.add(messagesList.get(i));
        }
        */
        int lastMesClient = Integer.parseInt(from);
        ChatLastMessages lmessages = new ChatLastMessages();
        lmessages =  getLMessages("TestPseudo.txt", "TestMessage.txt");
        ChatLastMessages toSend = new ChatLastMessages();
        ArrayList<ChatMessage> toSendList = new ArrayList<ChatMessage>();
        for(int i=lastMesClient; i<lmessages.getMessages().size(); i++) {
            toSendList.add(lmessages.getMessages().get(i));
        }
        toSend.setMessages(toSendList);
        //System.out.println("After file closed");
        if (lmessages!=null && lmessages.getMessages().size()==0) {
            return new ChatLastMessages();
        }
        //lmessages.setMessages(lastMessages);
        //System.out.println(messagesList.size());
        return toSend;
    }
    
    @GET
    @Path("/get")
    public String afficher() {
        return "We're connected !";
    }
    
    public static boolean addUser(ChatUser chU, String fileUser) {
        FileWriter outUser = null;
        BufferedWriter bUser ;
        try {
            File fu = new File(fileUser);
            if (!fu.exists())
                fu.createNewFile();
            outUser= new FileWriter(fileUser,true);
            bUser = new BufferedWriter(outUser);
            
            bUser.write(chU.getPseudo());
            bUser.newLine();
            System.out.println(chU.getPseudo());
            if (bUser!=null)
                bUser.close();
            /*
            if (outUser != null) {
                outUser.close();
            }*/
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public ArrayList<ChatUser> getUsers(String fileUser) {
        ArrayList<ChatUser> chUList = new ArrayList<ChatUser>();
        FileReader inUser = null;
        BufferedReader bUser ;
        String line = null;
        
        ChatLastMessages chLM = new ChatLastMessages();
        ArrayList<ChatMessage> chMList = new ArrayList<ChatMessage>();
        try {
            File fu = new File(fileUser);
            if (!fu.exists())
                fu.createNewFile();
            inUser = new FileReader(fileUser);
            bUser = new BufferedReader(inUser);

            
            //outPseudo.write(chM.getUser().getPseudo()+"|"+chM.getId());
            //outMessage.write(chM.getMessage());         
            String[] parts;
            while ((line = bUser.readLine()) != null) {
                //chM.setUser(new ChatUser(line));
                if (!line.equals(""))
                    chUList.add(new ChatUser(line));
            }
            
            if (bUser!=null)
                bUser.close();
            if (inUser != null) {
                inUser.close();
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return chUList;
    }
    
    public static boolean addMessage(ChatMessage chM, String filePseudo, String fileMessage) {
        
        FileWriter outPseudo = null;
        FileWriter outMessage = null;
        BufferedWriter buffWriterPseudo ;
        BufferedWriter buffWriterMessage ;
        try {
            File fp = new File(filePseudo);
            if (!fp.exists())
                fp.createNewFile();
            File fm = new File(fileMessage);
            if (!fm.exists())
                fm.createNewFile();
            outPseudo = new FileWriter(filePseudo,true);
            outMessage = new FileWriter(fileMessage,true);
            buffWriterPseudo = new BufferedWriter(outPseudo);
            buffWriterMessage = new BufferedWriter(outMessage);
            
            
            buffWriterPseudo.write(chM.getUser().getPseudo());
            buffWriterPseudo.newLine();
            buffWriterMessage.write(chM.getMessage());
            buffWriterMessage.newLine();
            
            if (buffWriterMessage!=null)
                buffWriterMessage.close();
            if (buffWriterPseudo!=null)
                buffWriterPseudo.close();
            
            if (outMessage != null) {
                outMessage.close();
            }
            if (outPseudo != null) {
                outPseudo.close();
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        /*
        try {
            File file = new File(filename);
            //file.createNewFile();
            FileOutputStream f = new FileOutputStream(file);
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(chM);

            o.close();
            f.close();
            
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
                return false;
            } catch (IOException e) {
                System.out.println("Error initializing stream");
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        return true;
       */
        return true;
    }
    
    public static ChatLastMessages getLMessages(String filePseudo, String fileMessage) {
        FileReader inPseudo = null;
        FileReader inMessage = null;
        BufferedReader buffReaderPseudo ;
        BufferedReader buffReaderMessage ;
        String line = null;
        
        ChatLastMessages chLM = new ChatLastMessages();
        ArrayList<ChatMessage> chMList = new ArrayList<ChatMessage>();
        try {
            File fp = new File(filePseudo);
            if (!fp.exists())
                fp.createNewFile();
            File fm = new File(fileMessage);
            if (!fm.exists())
                fm.createNewFile();
            inPseudo = new FileReader(filePseudo);
            
            inMessage = new FileReader(fileMessage);
            buffReaderPseudo = new BufferedReader(inPseudo);
            buffReaderMessage = new BufferedReader(inMessage);

            
            //outPseudo.write(chM.getUser().getPseudo()+"|"+chM.getId());
            //outMessage.write(chM.getMessage());         
            String[] parts;
            while ((line = buffReaderPseudo.readLine()) != null) {
                //chM.setUser(new ChatUser(line));
                if (!line.equals(""))
                    chMList.add(new ChatMessage(new ChatUser(line)));
            }
            int c=0;
            while ((line = buffReaderMessage.readLine()) != null) {
               //chM.setMessage(line);    
                if (!line.equals(""))
                    chMList.get(c).setMessage(line);
               //chMList.add(chM);
               c++;
            }
            chLM.setMessages(chMList);
            if (buffReaderMessage!=null)
                buffReaderMessage.close();
            if (buffReaderPseudo!=null)
                buffReaderPseudo.close();
            
            if (inPseudo != null) {
                inPseudo.close();
            }
            if (inMessage != null) {
                inMessage.close();
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ChatroomResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        ChatLastMessages chlmessages = null;
        try {
            File file = new File(filename);
            if (file.createNewFile())
                System.out.println("File created");
            else
                System.out.println("Failed creating file");
            
            System.out.println("After opening file");
            FileInputStream fi = new FileInputStream(file);
            System.out.println("NOON");
            ObjectInputStream oi = new ObjectInputStream(fi);
            System.out.println("NOON2");
            System.out.println("Available in oi stream :"+oi.available());
            if (oi.available()==0) {
                fi.close();
                oi.close();
                System.out.println("After ObjectInput Stream file closed");
                return new ChatLastMessages();
            }
            ArrayList<ChatMessage> listMessages = new ArrayList<ChatMessage>();
            // Read objects
            
            ChatMessage pr;
            int i=0;
            
            while ((pr = (ChatMessage)oi.readObject()) != null) {
                i++;
                System.out.println(pr.getMessage());
                listMessages.add(pr);
            }
            System.out.println("After Reading OI Stream file");
            chlmessages = new ChatLastMessages();
            //System.out.println(pr1.toString());
            //System.out.println(pr2.toString());

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return chlmessages;
        */
        return chLM;
    }
    
    
    public static ChatLastMessages getLMessagesV2(String filename) {
        ChatLastMessages chlmessages = new ChatLastMessages();
        String sRootPath = new File("a.txt").getAbsolutePath();
        System.out.println("Absolute path: "+sRootPath);
        try {
            File file = new File(filename);
            //file.createNewFile();            
            System.out.println("After opening file");
            FileInputStream fi = new FileInputStream(file);
            ObjectInputStream oi = new ObjectInputStream(fi);
            System.out.println("Available in oi stream :"+oi.available());
            /*if (oi.available()==0) {
                fi.close();
                oi.close();
                System.out.println("After ObjectInput Stream file closed");
                return new ChatLastMessages();
            }*/
            ArrayList<ChatMessage> listMessages = new ArrayList<ChatMessage>();
            // Read objects
            
            ChatMessage pr;
            int i=0;
            
            while ((pr = (ChatMessage)oi.readObject()) != null) {
                i++;
                //System.out.println(pr.getMessage());
                listMessages.add(pr);
            }
            System.out.println("After Reading OI Stream file");
            chlmessages = new ChatLastMessages();
            //System.out.println(pr1.toString());
            //System.out.println(pr2.toString());

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return chlmessages;
    }
}
