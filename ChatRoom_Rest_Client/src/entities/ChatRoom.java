/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class ChatRoom implements Serializable {
    
    private ArrayList<ChatUser> usersList = new ArrayList<ChatUser>();
    
    private ArrayList<ChatMessage> messageList = new ArrayList<ChatMessage>();

    public ArrayList<ChatUser> getUsersList() {
        return usersList;
    }

    public void addUsersList(ChatUser user) {
        this.usersList.add(user);
    }

    public ArrayList<ChatMessage> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<ChatMessage> messageList) {
        this.messageList = messageList;
    }

    
    @Override
    public String toString() {
        return "ChatRoom[ listUsers=" + usersList.toString() + " ]";
    }
    
}
