/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom_rest_client;

import com.google.gson.Gson;
import entities.ChatLastMessages;
import entities.ChatMessage;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Future;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import jdk.nashorn.internal.parser.JSONParser;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

/**
 *
 * @author admin
 */
public class ChatRoom_Rest_Client {

    private String title = "Logiciel de discussion en ligne";
    private JFrame window = new JFrame(this.title);
    public JTextArea txtOutput = new JTextArea();
    private JTextField txtMessage = new JTextField();
    private JButton btnSend = new JButton("Envoyer");

    private String pseudo = null;
    private static String baseURI = "http://localhost:8080/ChatRoom_Rest_V2/webresources/";
    private static int idLastMessage=0;
    
    
    public ChatRoom_Rest_Client() {
        this.createIHM();
        try{
            this.requestPseudo();       
        }        
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void createIHM() {
        // Assemblage des composants
        JPanel panel = (JPanel)this.window.getContentPane();
	JScrollPane sclPane = new JScrollPane(txtOutput);
	panel.add(sclPane, BorderLayout.CENTER);
        JPanel southPanel = new JPanel(new BorderLayout());
        southPanel.add(this.txtMessage, BorderLayout.CENTER);
        southPanel.add(this.btnSend, BorderLayout.EAST);
        panel.add(southPanel, BorderLayout.SOUTH);

        // Gestion des évènements
        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                window_windowClosing(e);
            }
        });
        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnSend_actionPerformed(e);
            }
        });
	/*txtMessage.addKeyListener(new KeyAdapter() {
	    public void keyReleased(KeyEvent event) {
		if (event.getKeyChar() == '\n')
		    btnSend_actionPerformed(null);
	    }
	});
        */

        // Initialisation des attributs
        this.txtOutput.setBackground(new Color(220,220,220));
	this.txtOutput.setEditable(false);
        this.window.setSize(500,400);
        this.window.setVisible(true);
        this.txtMessage.requestFocus();
    }

    public void requestPseudo() throws IOException  {
        this.pseudo = JOptionPane.showInputDialog(
                this.window, "Entrez votre pseudo : ",
                this.title,  JOptionPane.OK_OPTION
        );
        if (this.pseudo == null)
            System.exit(0);  
        else {
            String user = "{\"id\":0,\"pseudo\":\""+this.pseudo+"\"}";
            System.out.println(user);
            serverRequestPost("chatroom/subscribe", user);
        }     
    }

    public void window_windowClosing(WindowEvent e) {
	System.exit(-1);
    }

    public void btnSend_actionPerformed(ActionEvent e) {
        String message = this.txtMessage.getText();
        String json = "{\"pseudo\":\""+this.pseudo+"\", \"message\": \""+message+"\"}";
        System.out.println(json);
        serverRequestPost("chatroom/postMessage", json);
    }
    
    public void displayMessage(String message) {
        this.txtOutput.append( message + "\n");
    }
    
    public static boolean serverRequestPost(String link, String input) {
        try {
            URL url = new URL(baseURI+link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

            String output;
            
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return true;
    }
    public static String serverRequestGet(String link) {
        try {
            URL url = new URL(baseURI+link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                                    + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                   (conn.getInputStream())));

            String output;
            String outputReal = new String();
            //System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                outputReal += output;
            }
            //System.out.println(outputReal);
            
            //conn.disconnect();
            return outputReal;
            
        } catch (MalformedURLException e) {

              e.printStackTrace();
              return null;

        } catch (IOException e) {

              e.printStackTrace();
              return null;

        }
        
    }
    public static void main(String[] args){
        int i = 0;
        
        serverRequestGet("chatroom/get");
        /*
        try {

		URL url = new URL("http://localhost:8080/ChatRoom_Rest_V2/webresources/chatroom/get");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));

		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		conn.disconnect();

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	  }
        */
        
        try {
            ChatRoom_Rest_Client ch = new ChatRoom_Rest_Client();
            /*
            final Client client = new ResteasyClientBuilder()
                          .connectionPoolSize(3)
                          .build();
            WebTarget target = client.target("");
            Future<ChatLastMessages> future = target.path(baseURI+"chatroom/lastMessages/"+String.valueOf(idLastMessage))
                    .request()
                    .async()
                    .get(ChatLastMessages.class);
            ChatLastMessages chlm =  future.get();
            System.out.println(chlm.getMessages().size());
            */
            //WebTarget target = client.target(baseURI+"chatroom/lastMessages/"+String.valueOf(idLastMessage));
            
            System.out.println("Recuperation de nouveaux messages .... \n");
            boolean trouve = false;
            while(true) {
                String output = serverRequestGet("chatroom/lastMessages/"+String.valueOf(idLastMessage));
                
                //System.out.println("Recuperation de nouveaux messages v2 .... \n");
                System.out.println(output);
                //System.out.println("Recuperation de nouveaux messages v3 .... \n");
                Gson g = new Gson();
                ChatLastMessages p =  g.fromJson(output, ChatLastMessages.class);
                
                for (int j=0; j<p.getMessages().size(); j++){
                    ch.displayMessage(p.getMessages().get(j).getUser().getPseudo()+": "+p.getMessages().get(j).getMessage());
                    idLastMessage++;
                    System.out.println(p.getMessages().get(j).toString());
                }
            }
        }        
        catch(Exception e) {
            
        }
        
        
        /*
        try {   
            XmlRpcClient server = new XmlRpcClient("http://localhost:81/RPC2") ;
            ChatUserImpl ch = new ChatUserImpl(server);
            /*Vector params = new Vector();
            params.addElement(new Integer(17));
            params.addElement(new Integer(13));
            Object result = server.execute("sample.sum", params);
            int sum = ((Integer) result).intValue();
            System.out.println("The sum is: "+ sum);*/
            /*
            while(true){
                Vector result = (Vector) chR.execute("ChatRoom.getMessage", new Vector());
                if ((int)result.get(0)!=i){
                    ch.displayMessage(result.get(1).toString());
                    i = (int)result.get(0);
                }
            }

        } catch (Exception exception) {
            System.err.println("JavaClient: " + exception);
        }
        */
    }
}
